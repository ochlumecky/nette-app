<?php

declare(strict_types=1);

namespace App\Dto;

use Nette\Utils\DateTime;

class CreateUserDto
{
    public function __construct(
        private string $firstName,
        private string $lastName,
        private DateTime $yearOfBirth
    )
    {
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getYearOfBirth(): DateTime
    {
        return $this->yearOfBirth;
    }
}