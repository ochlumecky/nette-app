<?php

declare(strict_types=1);

namespace App\Collections;

use App\Entity\UserEntity;

class UserCollections
{
    private array $users = [];

    public function addUser(UserEntity $entity): void
    {
        $this->users[] = $entity;
    }

    public function getUsers(): array
    {
        return $this->users;
    }
}