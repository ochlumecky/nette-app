
# Zadání:
- V prostředí skeletonové aplikace Nette Frameworku (https://github.com/nette/web-project#installation) vytvořte jednoduchý modul s MVC architekturou.
- Modul bude obsluhovat CRUD operace nad jednoduchým objektem (napojení na zdroj dat může být naznačeno mockováním).
- Pro Read bude využitý presenter
- Pro Create a Update bude využita nette/forms komponenta
- Pro Delete bude využita akce nebo handler presenteru
- Jako bonus můžete vytvořit unit testy pro model buď s pomocí PHPUnit, nebo nette/tester

# Routs

### Create/update - nette forms 
### Read - Presenter **
### Delete - Action or Handler 