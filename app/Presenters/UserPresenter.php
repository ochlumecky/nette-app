<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Dto\CreateUserDto;
use App\Dto\DeleteUserDto;
use App\Dto\EditUserDto;
use App\Dto\GetUserDto;
use App\Entity\UserEntity;
use App\Model\UserModel;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Utils\DateTime;

class UserPresenter extends Presenter
{

    public function __construct(private UserModel $userModel)
    {
    }

    public function renderDefault(): void
    {
        $this->template->users = $this->userModel->getAllUsers()->getUsers();
    }

    protected function createComponentUserForm(): Form
    {
        $form = new Form;

        $form->addText('firstName', 'Jméno:')
            ->setRequired();

        $form->addText('lastName', 'Příjmení:');

        $form->addText('dateOfBirth', 'Datum narození:')
            ->setHtmlType('date')
            ->setRequired();

        $form->addSubmit('save', 'Uložit uživatele');
        $form->onSuccess[] = [$this, 'createUserFormSuccess'];
        return $form;
    }

    public function renderEditUser(int $userId): void
    {
        $result = $this->userModel->getOneUser(new GetUserDto(id: $userId));

        if (!$result instanceof UserEntity) {
            $this->error('User not found');
        }

        $this->getComponent('userForm')
            ->setDefaults($result->toArray());
    }

    #[NoReturn]
    public function createUserFormSuccess(\stdClass $data): void
    {
        $userId = $this->getParameter('userId');

        if ($userId) {
            $this->userModel->updateUser(
                new EditUserDto(
                    id:          (int)$userId,
                    firstName:   $data->firstName,
                    lastName:    $data->lastName,
                    yearOfBirth: new DateTime($data->dateOfBirth)
                )
            );
        }
        else
        {
            $this->userModel->saveNewUser(
                new CreateUserDto(
                    firstName:   $data->firstName,
                    lastName:    $data->lastName,
                    yearOfBirth: new DateTime($data->dateOfBirth)
                )
            );
        }
        $this->redirect('default');
    }

    #[NoReturn]
    public function actionDeleteUser(int $userId)
    {
        $result = $this->userModel->removeUser(new DeleteUserDto($userId));
        if (!$result) {
            $this->error('User not delete');
        }
        $this->redirect('default');
    }


}