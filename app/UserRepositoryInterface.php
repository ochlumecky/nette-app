<?php

namespace App;

use App\Collections\UserCollections;
use App\Entity\UserEntity;

interface UserRepositoryInterface
{
    public function saveEntity(UserEntity $entity): void;

    public function getEntities(): UserCollections;

    public function getEntity(int $id): ?UserEntity;

    public function updateEntity(UserEntity $entity): void;

    public function  deleteEntity(UserEntity $entity): ?int;

}