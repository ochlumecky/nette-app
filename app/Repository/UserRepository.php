<?php

declare(strict_types=1);

namespace App\Repository;

use App\Collections\UserCollections;
use App\Entity\UserEntity;
use App\UserRepositoryInterface;
use Nette\Database\Explorer;
use Nette\Database\Table\ActiveRow;

class UserRepository implements UserRepositoryInterface
{
    public function __construct(
        private Explorer $database,
    ) {
    }

    public function saveEntity(UserEntity $entity): void
    {
        $this->database->table('users')->insert([
                                                    'first_name' => $entity->getFirstName(),
                                                    'last_name' => $entity->getLastName(),
                                                    'date_birth' => $entity->getDateOfBirth()
                                                ]);
    }

    public function getEntities(): UserCollections
    {
        $result = $this->database->table('users')->fetchAll();
        $collection = new UserCollections();

        foreach ($result as $item) {
            $collection->addUser(
                new UserEntity(
                    firstName:   $item->first_name,
                    lastName:    $item->last_name,
                    dateOfBirth: $item->date_birth,
                    id:          $item->id,
                    deleteAt:    $item->deleted_at
                )
            );
        }
        return $collection;
    }

    public function updateEntity(UserEntity $entity): void
    {
       $this->database->table('users')->get($entity->getId())->update([
                                                                          'first_name' => $entity->getFirstName(),
                                                                          'last_name' => $entity->getLastName(),
                                                                          'date_birth' => $entity->getDateOfBirth()
                                                                      ]);
    }


    public function getEntity(int $id): ?UserEntity
    {
       $result = $this->database->table('users')->get($id);

       if($result instanceof ActiveRow)
       {
          return new UserEntity(
              firstName:   $result->first_name,
              lastName:    $result->last_name,
              dateOfBirth: $result->date_birth,
              id:          $result->id,
              deleteAt:    $result->deleted_at
          );
       }

       return null;
    }

    public function deleteEntity(UserEntity $entity): ?int
    {
       return $this->database->table('users')->where('id',$entity->getId())->delete();
    }
}