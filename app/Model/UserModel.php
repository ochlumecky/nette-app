<?php

declare(strict_types=1);

namespace App\Model;

use App\Collections\UserCollections;
use App\Dto\CreateUserDto;
use App\Dto\DeleteUserDto;
use App\Dto\EditUserDto;
use App\Dto\GetUserDto;
use App\Entity\UserEntity;
use App\UserRepositoryInterface;

class UserModel
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function getAllUsers(): UserCollections
    {
        return $this->userRepository->getEntities();
    }

    public function saveNewUser(CreateUserDto $dto): void
    {
        $this->userRepository->saveEntity(
            new UserEntity(
                firstName:   $dto->getFirstName(),
                lastName:    $dto->getLastName(),
                dateOfBirth: $dto->getYearOfBirth()
            )
        );
    }

    public function getOneUser(GetUserDto $dto): ?UserEntity
    {
        return $this->userRepository->getEntity($dto->getId());
    }

    public function updateUser(EditUserDto $dto): void
    {
        $this->userRepository->updateEntity(
            new UserEntity(
                firstName:   $dto->getFirstName(),
                lastName:    $dto->getLastName(),
                dateOfBirth: $dto->getYearOfBirth(),
                id:          $dto->getId()
            )
        );
    }

    public function removeUser(DeleteUserDto $dto): ?int
    {
        $user = $this->userRepository->getEntity($dto->getId());

        if(!$user instanceof UserEntity)
        {
           return null;
        }
       return $this->userRepository->deleteEntity($user);
    }
}