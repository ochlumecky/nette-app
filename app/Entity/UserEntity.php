<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;

class UserEntity
{
    public function __construct(
       private string $firstName,
       private string $lastName,
       private DateTime $dateOfBirth,
        private ?int $id = null,
       private ?DateTime $deleteAt = null
    ) {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getDateOfBirth(): string
    {
        return $this->dateOfBirth->format('d.m.Y');
    }

    public function getDeleteAt(): ?DateTime
    {
        return $this->deleteAt;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'dateOfBirth' => $this->getDateOfBirth(),
        ];
    }
}