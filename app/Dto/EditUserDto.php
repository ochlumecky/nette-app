<?php

declare(strict_types=1);

namespace App\Dto;

use Nette\Utils\DateTime;

class EditUserDto
{

    public function __construct(
        private int $id,
        private string $firstName,
        private string $lastName,
        private DateTime $yearOfBirth)
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getYearOfBirth(): DateTime
    {
        return $this->yearOfBirth;
    }
}